'use strict';

const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');
const merge = require('webpack-merge');

const webpackCommon = {
    entry: {
        app: ['./app/app.js']
    },
    module: {
        rules: [
            {
                test: /\.js?$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader?presets[]=env'
                    }
                ]
            },
            {
                test: /\.ejs$/,
                exclude: /node_modules/,
                use: {
                    loader: 'underscore-template-loader',
                    query: {
                        engine: 'lodash',
                        variable: 'data'
                    }
                }
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: 'css-loader'
                })
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'url-loader?limit=10000&mimetype=application/font-woff'
            },
            {
                test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'file-loader'
            }
        ]
    },
    output: {
        filename: 'app.js',
        path: path.join(__dirname, './public'),
        publicPath: '/'
    },
    plugins: [
        new ExtractTextPlugin('app.css'),
        new CopyWebpackPlugin([{
            from: './app/assets/index.html',
            to: './index.html'
        }]),
        new CopyWebpackPlugin([{
            from: './app/assets/public',
            to: './assets/'
        }]),
        new webpack.ProvidePlugin({
            $: 'jquery',
            _: 'underscore'
        })
    ],
    resolve: {
        modules: [
            path.join(__dirname, './node_modules'),
            path.join(__dirname, './app')
        ]
    },
    resolveLoader: {
        modules: [
            path.join(__dirname, './node_modules')
        ]
    }
};

switch (process.env.npm_lifecycle_event) {
    case 'start':
    case 'dev':
        module.exports = merge(webpackCommon, {
            devtool: '#inline-source-map',
            devServer: {
                contentBase: path.join(__dirname, 'public'),
                compress: true,
                port: 9000
            }
        });
        break;
    default:
        module.exports = merge(webpackCommon, {
            devtool: 'source-map'
        });
        break;
}
