import Marionette from 'backbone.marionette';

import template from 'templates/SearchView.ejs';

export const SearchView = Marionette.ItemView.extend({
    template,
    ui: {
        form: '.search-form',
        input: '.search-input'
    },
    events: {
        'submit @ui.form': () => false,
        'keyup @ui.input': 'onInputChange'
    },

    onInputChange(e) {
        const searchString = e.currentTarget.value;
        this.triggerMethod('search:input:change', searchString);
    }
});
