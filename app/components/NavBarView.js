import Marionette from 'backbone.marionette';

import template from 'templates/NavBarView.ejs';

export const NavBarView = Marionette.ItemView.extend({
    template
});
