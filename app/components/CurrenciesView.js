import _ from "lodash";
import Marionette from 'backbone.marionette';
import { CurrencyView } from 'components/CurrencyView';
import { Currencies } from "../models/Currencies";

export const FILTER_INTERVAL = 400;
const filterDebounced = _.debounce(filter, FILTER_INTERVAL);

export function filter(view, searchString) {
    view.filter = (currency) => {
        const code = currency.get('currency');
        const name = currency.get('nameI18N');
        const searchStringLowercase = searchString.toLowerCase();
        return (code && code.toLowerCase().includes(searchStringLowercase))
            || (name && name.toLowerCase().includes(searchStringLowercase));
    };
    view.render();
}

export const CurrenciesView = Marionette.CollectionView.extend({
    tagName: 'ul',
    className: 'list-group',
    childView: CurrencyView,
    collection: new Currencies(),

    filterCurrencies(searchString) {
        filterDebounced(this, searchString);
    },

    onBeforeRender() {
        this.collection.fetch();
    }
});
