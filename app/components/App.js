import Marionette from 'backbone.marionette';
import { LayoutView } from './LayoutView';

export class App extends Marionette.Application {
    onStart() {
        this.rootView = new LayoutView();
        this.rootView.render();
    }
}
