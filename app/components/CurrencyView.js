import Marionette from 'backbone.marionette';
import _ from 'lodash';
import { isExchangeRateMovingDown, isExchangeRateMovingUp, getCountryByCurrency } from 'helpers/currency';
import { getFlagUrlForCountry } from 'helpers/country';

import template from 'templates/CurrencyView.ejs';

const currencyFormatter = new Intl.NumberFormat('de-DE', {
    style: 'currency',
    currency: 'EUR',
    currencyDisplay: 'code',
    minimumFractionDigits: 4,
    maximumFractionDigits: 4
});

export function formatExchangeRate(exchangeRate) {
    return currencyFormatter.format(exchangeRate).padStart(16, ' ');
}

export function getCurrencyName(currency) {
    return _.get(currency, 'nameI18N', 'Unknown currency');
}

export function getCountryName(currency) {
    const country = getCountryByCurrency(currency);
    return _.get(country, 'name.common', 'Unknown country');
}

export function getCountryFlag(currency) {
    const country = getCountryByCurrency(currency);
    return getFlagUrlForCountry(country);
}

export function setDefaultCountryFlag(view) {
    view.ui.flag.prop('src', '/assets/flags/default.png');
    view.ui.flag.off('error');
}

export const CurrencyView = Marionette.ItemView.extend({
    template,
    tagName: 'li',
    className: 'currency list-group-item d-flex justify-content-between align-items-center',
    ui: {
        flag: '.country-flag'
    },
    templateHelpers: {
        formatExchangeRate,
        isExchangeRateMovingUp,
        isExchangeRateMovingDown,
        getCurrencyName,
        getCountryName,
        getCountryFlag
    },

    onRender() {
        this.ui.flag.on('error', setDefaultCountryFlag.bind(null, this));
    },

    onBeforeDestroy() {
        this.ui.flag.off('error');
    }
});
