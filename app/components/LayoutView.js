import Marionette from 'backbone.marionette';
import Stickyfill from 'stickyfilljs';
import { NavBarView } from './NavBarView';
import { SearchView } from './SearchView';
import { CurrenciesView } from './CurrenciesView';

import template from 'templates/LayoutView.ejs';

export const LayoutView = Marionette.LayoutView.extend({
    template,
    el: 'body',
    regions: {
        navBarRegion: '.nav-bar-region',
        searchRegion: '.search-region',
        currenciesRegion: '.currencies-region'
    },
    childEvents: {
        'search:input:change': 'onSearchInputChange'
    },

    onSearchInputChange(childView, searchString) {
        const currenciesView = this.currenciesRegion.currentView;
        currenciesView.filterCurrencies(searchString);
    },

    onBeforeRender() {
        this.searchRegion.on('show', () => {
            Stickyfill.add(this.searchRegion.el);
        });

        this.on('before:remove:region', (name) => {
            if(name === 'searchRegion') {
                Stickyfill.remove(this.searchRegion.el);
            }
        });
    },

    onRender() {
        this.showChildView('navBarRegion', new NavBarView());
        this.showChildView('searchRegion', new SearchView());
        this.showChildView('currenciesRegion', new CurrenciesView());
    }
});
