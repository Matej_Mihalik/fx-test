import _ from 'lodash';

import countries from 'assets/countries';

export function isExchangeRateMovingUp(currencyModel) {
    return _.get(currencyModel, 'exchangeRate.indicator', 0) > 0;
}

export function isExchangeRateMovingDown(currencyModel) {
    return _.get(currencyModel, 'exchangeRate.indicator', 0) < 0;
}

export function getCountryByCurrency(currencyModel) {
    return countries.find(({ currency }) => currency.includes(currencyModel.currency));
}
