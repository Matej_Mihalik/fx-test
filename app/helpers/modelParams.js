import _ from 'lodash';

export function whitelist(obj, params) {
    const result = {};

    if (_.isEmpty(obj)) {
        return result;
    }

    params.forEach(param => {
        if (_.isArray(param)) {
            const [innerParam, ...innerWhitelist] = param;
            result[innerParam] = whitelist(obj[innerParam], innerWhitelist);
        }
        else if (obj.hasOwnProperty(param)) {
            result[param] = obj[param];
        }
    });

    return result;
}
