import _ from 'lodash';

export function getFlagUrlForCountry(country) {
    const countryCode = _.get(country, 'cca2', 'default').toLowerCase();
    return `/assets/flags/${countryCode}.png`;
}
