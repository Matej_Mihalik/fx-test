import { App } from 'components/App';

import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-free/css/all.css';
import 'styles/app.css';

document.addEventListener('DOMContentLoaded', () => {
    const app = new App();
    app.start();
});
