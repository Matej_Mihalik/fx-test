import _ from 'lodash';
import Backbone from 'backbone';
import { Currency } from 'models/Currency'

export class Currencies extends Backbone.Collection {
    constructor(options) {
        super(options);
        this.url = 'http://localhost:3000/at-fat/fat1/rest/info/fx';
        this.model = Currency;
    }

    parse(response) {
        return response.fx.filter(fx => !_.isEmpty(fx.exchangeRate));
    }
}
