import Backbone from 'backbone';
import { whitelist } from 'helpers/modelParams';

export const whitelistParams = [
    'currency',
    'nameI18N', [
        'exchangeRate',
        'buy',
        'sell',
        'indicator',
        'lastModified'
    ]
];

export class Currency extends Backbone.Model {
    constructor(attributes, options) {
        super(attributes, options);
        this.idAttribute = 'currency';
    }

    parse(response) {
        return whitelist(response, whitelistParams);
    }
}
