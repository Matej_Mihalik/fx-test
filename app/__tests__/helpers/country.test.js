import _ from 'lodash';
import { getFlagUrlForCountry } from 'helpers/country';

import countries from 'assets/countries';

const USA_FLAG_PATH_REGEX = /\/us\.png$/;
const DEFAULT_FLAG_PATH_REGEX = /\/default\.png$/;

function getUSA() {
    return countries.find(country => _.get(country, 'name.common') === 'United States');
}

function getCountryWithoutCode() {
    const USA = getUSA();
    const countryWithoutCode = _.cloneDeep(USA);
    delete countryWithoutCode.cca2;
    return countryWithoutCode;
}

describe('country', () => {
    describe('getFlagUrlForCountry', () => {
        it('should return path to correct country flag', () => {
            const USA = getUSA();

            const flagPath = getFlagUrlForCountry(USA);

            expect(flagPath).toMatch(USA_FLAG_PATH_REGEX);
        });

        it('should return path to default flag if country has no valid code', () => {
            const countryWithoutCode = getCountryWithoutCode();

            const flagPath = getFlagUrlForCountry(countryWithoutCode);

            expect(flagPath).toMatch(DEFAULT_FLAG_PATH_REGEX);
        });

        it('should return path to default flag if no country provided', () => {
            const flagPath = getFlagUrlForCountry();

            expect(flagPath).toMatch(DEFAULT_FLAG_PATH_REGEX);
        });
    });
});
