import _ from 'lodash';
import { whitelist } from 'helpers/modelParams';

import currencyFixture from '__fixtures__/currency';

const WHITELIST_PARAMS = ['currency', 'nameI18N'];
const WHITELIST_PARAMS_NESTED = ['currency', 'nameI18N', ['exchangeRate', 'buy', 'sell']];
const WHITELIST_PARAMS_INVALID = ['currency', 'nonsense'];
const WHITELIST_PARAMS_NONE = [];


describe('modelParams', () => {
    describe('whitelist', () => {
        it('should return only whitelisted params', () => {
            const expectedObj = _.pick(currencyFixture, WHITELIST_PARAMS);

            const whitelisted = whitelist(currencyFixture, WHITELIST_PARAMS);

            expect(whitelisted).toEqual(expectedObj);
        });

        it('should support nested params', () => {
            const expectedObj = _.pick(currencyFixture,  ['currency', 'nameI18N']);
            expectedObj.exchangeRate = _.pick(currencyFixture.exchangeRate, ['buy', 'sell']);

            const whitelisted = whitelist(currencyFixture, WHITELIST_PARAMS_NESTED);

            expect(whitelisted).toEqual(expectedObj);
        });

        it('should ignore invalid params', () => {
            const expectedObj = _.pick(currencyFixture, 'currency');

            const whitelisted = whitelist(currencyFixture, WHITELIST_PARAMS_INVALID);

            expect(whitelisted).toEqual(expectedObj);
        });

        it('should return an empty object when no params supplied', () => {
            const expectedObj = {};

            const whitelisted = whitelist(currencyFixture, WHITELIST_PARAMS_NONE);

            expect(whitelisted).toEqual(expectedObj);
        });

        it('should return an empty object when supplied with empty model', () => {
            const expectedObj = {};

            const whitelisted = whitelist({}, WHITELIST_PARAMS);

            expect(whitelisted).toEqual(expectedObj);
        });
    });
});
