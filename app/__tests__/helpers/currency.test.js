import _ from 'lodash';
import { isExchangeRateMovingUp, isExchangeRateMovingDown, getCountryByCurrency } from 'helpers/currency';

import countries from 'assets/countries';
import currencyFixture from '__fixtures__/currency';

function getNegativeCurrencyFixture() {
    const currencyFixtureNegative = _.cloneDeep(currencyFixture);
    currencyFixtureNegative.exchangeRate.indicator = -1;
    return currencyFixtureNegative;
}

function getNeutralCurrencyFixture() {
    const currencyFixtureNeutral = _.cloneDeep(currencyFixture);
    currencyFixtureNeutral.exchangeRate.indicator = 0;
    return currencyFixtureNeutral;
}

function getAmericanSamoa() {
    return countries.find(country => _.get(country, 'name.common') === 'American Samoa');
}

describe('currency', () => {
    describe('isExchangeRateMovingUp', () => {
        it('should return true if indicator is positive', () => {
            const result = isExchangeRateMovingUp(currencyFixture);

            expect(result).toEqual(true);
        });

        it('should return false if indicator is negative', () => {
            const negativeCurrency = getNegativeCurrencyFixture();

            const result = isExchangeRateMovingUp(negativeCurrency);

            expect(result).toEqual(false);
        });

        it('should return false if indicator is neutral', () => {
            const neutralCurrency = getNeutralCurrencyFixture();

            const result = isExchangeRateMovingUp(neutralCurrency);

            expect(result).toEqual(false);
        });
    });

    describe('isExchangeRateMovingDown', () => {
        it('should return false if indicator is positive', () => {
            const result = isExchangeRateMovingDown(currencyFixture);

            expect(result).toEqual(false);
        });

        it('should return true if indicator is negative', () => {
            const negativeCurrency = getNegativeCurrencyFixture();

            const result = isExchangeRateMovingDown(negativeCurrency);

            expect(result).toEqual(true);
        });

        it('should return false if indicator is neutral', () => {
            const neutralCurrency = getNeutralCurrencyFixture();

            const result = isExchangeRateMovingDown(neutralCurrency);

            expect(result).toEqual(false);
        });
    });

    describe('getCountryByCurrency', () => {
        it('should return the first country using given currency', () => {
            const foundCountry = getCountryByCurrency(currencyFixture);
            const americanSamoa = getAmericanSamoa();

            expect(foundCountry).toEqual(americanSamoa);
        });
    });
});
