import _ from 'lodash';
import { Currencies } from 'models/Currencies';

import currenciesFixture from '__fixtures__/currencies';

const ALL_CURRENCIES_LENGTH = 2;
const currencies = new Currencies();

describe('Currencies', () => {
    it('should break out from "fx" key during parse', () => {
        const parsedCurrencies = currencies.parse(currenciesFixture);

        expect(parsedCurrencies).toHaveLength(ALL_CURRENCIES_LENGTH);
    });

    it('should filter out currencies without exchange rate data during parse', () => {
        const currenciesFixtureWithoutExchangeData = _.cloneDeep(currenciesFixture);
        delete _.last(currenciesFixtureWithoutExchangeData.fx).exchangeRate;

        const parsedCurrencies = currencies.parse(currenciesFixtureWithoutExchangeData);

        expect(parsedCurrencies).toHaveLength(ALL_CURRENCIES_LENGTH - 1);
    });
});
