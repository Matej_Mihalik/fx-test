import { Currency, whitelistParams } from 'models/Currency';
import { whitelist } from 'helpers/modelParams';

import currencyFixture from '__fixtures__/currency';

const currency = new Currency();

describe('Currency', () => {
    it('should whitelist params during parse', () => {
        const whitelisted = whitelist(currencyFixture, whitelistParams);

        const parsedCurrency = currency.parse(currencyFixture);

        expect(parsedCurrency).toEqual(whitelisted);
    });
});
