import { CurrenciesView, FILTER_INTERVAL } from 'components/CurrenciesView'

import currenciesFixture from '__fixtures__/currencies';

const ALL_CURRENCIES_LENGTH = 2;
const FILTERED_CURRENCIES_LENGTH = 1;
const SEARCH_STRING_BY_CODE = 'USD';
const SEARCH_STRING_BY_NAME = 'peru';
const currenciesView = new CurrenciesView();

function loadCollection() {
    const parsedCurrencies = currenciesView.collection.parse(currenciesFixture);
    currenciesView.collection.reset(parsedCurrencies);
}

describe('CurrenciesView', () => {
    it('should fetch the collection on render', () => {
        const fetchSpy = jest.spyOn(currenciesView.collection, 'fetch');

        currenciesView.render();

        expect(fetchSpy).toHaveBeenCalled();
        loadCollection();
    });

    it('should filter currencies by currency code', (done) => {
        const currencyEls = currenciesView.el.querySelectorAll('.currency');
        expect(currencyEls).toHaveLength(ALL_CURRENCIES_LENGTH);

        currenciesView.filterCurrencies(SEARCH_STRING_BY_CODE);

        setTimeout(() => {
            const currencyEls = currenciesView.el.querySelectorAll('.currency');
            expect(currencyEls).toHaveLength(FILTERED_CURRENCIES_LENGTH);
            expect(currencyEls[0]).toHaveTextContent(SEARCH_STRING_BY_CODE);
            currenciesView.filterCurrencies('');
            setTimeout(done, FILTER_INTERVAL + 1);
        }, FILTER_INTERVAL + 1);
    });

    it('should filter currencies by currency name', (done) => {
        const currencyEls = currenciesView.el.querySelectorAll('.currency');
        expect(currencyEls).toHaveLength(ALL_CURRENCIES_LENGTH);

        currenciesView.filterCurrencies(SEARCH_STRING_BY_NAME);

        setTimeout(() => {
            const currencyEls = currenciesView.el.querySelectorAll('.currency');
            expect(currencyEls).toHaveLength(FILTERED_CURRENCIES_LENGTH);
            expect(currencyEls[0]).toHaveTextContent(SEARCH_STRING_BY_NAME);
            currenciesView.filterCurrencies('');
            setTimeout(done, FILTER_INTERVAL + 1);
        }, FILTER_INTERVAL + 1);
    });
});
