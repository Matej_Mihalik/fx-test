import _ from "lodash";
import $ from 'jquery';
import { CurrencyView, formatExchangeRate, getCurrencyName, getCountryName, getCountryFlag, setDefaultCountryFlag } from 'components/CurrencyView'
import { Currency } from 'models/Currency';

import currencyFixture from '__fixtures__/currency';

const FIXTURE_CURRENCY_NAME = 'US Dollar';
const DEFAULT_CURRENCY_NAME = 'Unknown currency';
const FIXTURE_COUNTRY_NAME = 'American Samoa';
const DEFAULT_COUNTRY_NAME = 'Unknown country';
const FIXTURE_BUY_EXCHANGE_RATE = 1.3817;
const FIXTURE_SELL_EXCHANGE_RATE = 1.3917;
const SAMOA_FLAG_PATH_REGEX = /\/as\.png$/;
const DEFAULT_FLAG_PATH_REGEX = /\/default\.png$/;
const NEUTRAL_CURRENCY_INDICATOR = '‐';
const EXCHANGE_RATE = 1;
const CONVERTED_CURRENCY_CODE = 'EUR';
const currencyView = new CurrencyView();

function getNegativeCurrencyFixture() {
    const currencyFixtureNegative = _.cloneDeep(currencyFixture);
    currencyFixtureNegative.exchangeRate.indicator = -1;
    return currencyFixtureNegative;
}

function getNeutralCurrencyFixture() {
    const currencyFixtureNeutral = _.cloneDeep(currencyFixture);
    currencyFixtureNeutral.exchangeRate.indicator = 0;
    return currencyFixtureNeutral;
}

function getCurrencyFixtureWithoutName() {
    const currencyFixtureWithoutName = _.cloneDeep(currencyFixture);
    delete currencyFixtureWithoutName.nameI18N;
    return currencyFixtureWithoutName;
}

function getCurrencyFixtureWithoutCountry() {
    const currencyFixtureWithoutCountry = _.cloneDeep(currencyFixture);
    currencyFixtureWithoutCountry.currency = 'XXX';
    return currencyFixtureWithoutCountry;
}

beforeAll(() => {
    currencyView.model = new Currency(currencyFixture);
    currencyView.render();
});

describe('CurrencyView', () => {
    describe('formatExchangeRate', () => {
        it(`should format number to ${CONVERTED_CURRENCY_CODE} currency`, () => {
            const formattedExchangeRate = formatExchangeRate(EXCHANGE_RATE);

            expect(formattedExchangeRate).toMatch(CONVERTED_CURRENCY_CODE);
        });

        it('should round number to 4 decimal places', () => {
            const formattedExchangeRate = formatExchangeRate(EXCHANGE_RATE);

            expect(formattedExchangeRate).toMatch(/[0-9]*,[0-9]{4}/);
        });

        it('should pad number to 16 characters', () => {
            const formattedExchangeRate = formatExchangeRate(EXCHANGE_RATE);

            expect(formattedExchangeRate).toHaveLength(16);
        });
    });

    describe('getCurrencyName', () => {
        it('should return full currency name', () => {
            const currencyName = getCurrencyName(currencyFixture);

            expect(currencyName).toBe(FIXTURE_CURRENCY_NAME);
        });

        it('should return default currency name, when none available', () => {
            const currencyFixtureWithoutName = getCurrencyFixtureWithoutName();

            const countryName = getCurrencyName(currencyFixtureWithoutName);

            expect(countryName).toBe(DEFAULT_CURRENCY_NAME);
        });
    });

    describe('getCountryName', () => {
        it('should return country name', () => {
            const countryName = getCountryName(currencyFixture);

            expect(countryName).toBe(FIXTURE_COUNTRY_NAME);
        });

        it('should return default country name, if none available', () => {
            const currencyFixtureWithoutCountry = getCurrencyFixtureWithoutCountry();

            const countryName = getCountryName(currencyFixtureWithoutCountry);

            expect(countryName).toBe(DEFAULT_COUNTRY_NAME);
        });
    });

    describe('getCountryFlag', () => {
        it('should return path to country flag', () => {
            const flagPath = getCountryFlag(currencyFixture);

            expect(flagPath).toMatch(SAMOA_FLAG_PATH_REGEX);
        });

        it('should return path to default country flag, if currency has no country', () => {
            const currencyFixtureWithoutCountry = getCurrencyFixtureWithoutCountry();

            const flagPath = getCountryFlag(currencyFixtureWithoutCountry);

            expect(flagPath).toMatch(DEFAULT_FLAG_PATH_REGEX);
        });
    });

    describe('setDefaultCountryFlag', () => {
        it('should set default country flag', () => {
            const tmpCurrencyView = new CurrencyView({
                model: new Currency(currencyFixture)
            });
            tmpCurrencyView.render();

            setDefaultCountryFlag(tmpCurrencyView);
            const flagPath = tmpCurrencyView.ui.flag.attr('src');

            expect(flagPath).toMatch(DEFAULT_FLAG_PATH_REGEX);
        });

        it('should unbind itself after execution', () => {
            const tmpCurrencyView = new CurrencyView({
                model: new Currency(currencyFixture)
            });
            tmpCurrencyView.render();
            const flagEl = tmpCurrencyView.ui.flag.get(0);
            const eventListeners = $._data(flagEl, 'events');
            expect(eventListeners.error).toHaveLength(1);

            setDefaultCountryFlag(tmpCurrencyView);

            expect(eventListeners.error).toBeUndefined();
        });
    });

    describe('View', () => {
        it('should not fail when initialized without model', () => {
            expect(() => {
                new CurrencyView();
            }).not.toThrow();
        });

        it('should bind flag error listeners on render', () => {
            const flagEl = currencyView.ui.flag.get(0);
            const eventListeners = $._data(flagEl, 'events');

            expect(eventListeners.error).toHaveLength(1);
        });

        it('should unbind flag error listeners on destroy', () => {
            const tmpCurrencyView = new CurrencyView({
                model: new Currency(currencyFixture)
            });
            tmpCurrencyView.render();
            const flagEl = tmpCurrencyView.ui.flag.get(0);
            const eventListeners = $._data(flagEl, 'events');
            expect(eventListeners.error).toHaveLength(1);

            tmpCurrencyView.destroy();

            expect(eventListeners.error).toBeUndefined();
        });

        it('should display country flag', () => {
            const flag = currencyView.el.querySelector('.country-flag');

            expect(flag).not.toBeNull();
        });

        it('should display currency code', () => {
            expect(currencyView.el).toHaveTextContent(currencyFixture.currency);
        });

        it('should display upward currency movement indicator if currency movement is positive', () => {
            const indicator = currencyView.el.querySelector('.fa-long-arrow-alt-up');

            expect(indicator).not.toBeNull();
        });

        it('should display downward currency movement indicator if currency movement is negative', () => {
            const negativeCurrencyFixture = getNegativeCurrencyFixture();
            currencyView.model.set(negativeCurrencyFixture);

            currencyView.render();
            const indicator = currencyView.el.querySelector('.fa-long-arrow-alt-down');

            expect(indicator).not.toBeNull();
        });

        it('should display a dash if currency movement is neutral', () => {
            const neutralCurrencyFixture = getNeutralCurrencyFixture();
            currencyView.model.set(neutralCurrencyFixture);

            currencyView.render();

            expect(currencyView.el).toHaveTextContent(NEUTRAL_CURRENCY_INDICATOR);
        });

        it('should display currency name', () => {
            expect(currencyView.el).toHaveTextContent(FIXTURE_CURRENCY_NAME);
        });

        it('should display country name', () => {
            expect(currencyView.el).toHaveTextContent(FIXTURE_COUNTRY_NAME);
        });

        it('should display formatted buy exchange rate', () => {
            const expectedExchangeRate = formatExchangeRate(FIXTURE_BUY_EXCHANGE_RATE);

            expect(currencyView.el).toHaveTextContent(expectedExchangeRate);
        });

        it('should display formatted sell exchange rate', () => {
            const expectedExchangeRate = formatExchangeRate(FIXTURE_SELL_EXCHANGE_RATE);

            expect(currencyView.el).toHaveTextContent(expectedExchangeRate);
        });
    });
});

