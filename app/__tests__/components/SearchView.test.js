import { SearchView } from 'components/SearchView'

const SEARCH_STRING = 'search';
const TRIGGERED_EVENT_NAME = 'search:input:change';
const searchView = new SearchView();

beforeAll(() => {
    searchView.render();
});

describe('SearchView', () => {
    it('should prevent search form submission', () => {
        const form = searchView.ui.form;
        const submitSpy = jest.spyOn(form.get(0), 'submit');

        form.trigger('submit');

        expect(submitSpy).not.toHaveBeenCalled();
    });

    it(`should trigger ${TRIGGERED_EVENT_NAME} event passing input value on keyup`, () => {
        const input = searchView.ui.input;
        const triggerMethodSpy = jest.spyOn(searchView, 'triggerMethod');

        input.val(SEARCH_STRING).trigger('keyup');

        expect(triggerMethodSpy).toHaveBeenCalledWith(TRIGGERED_EVENT_NAME, SEARCH_STRING);
    });
});
