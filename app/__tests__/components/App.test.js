import { App } from 'components/App';
import { LayoutView } from 'components/LayoutView';

const app = new App();

beforeAll(() => {
    app.start();
});

describe('App', () => {
    it('should set LayoutView as it\'s rootView', () => {
        expect(app.rootView).toBeInstanceOf(LayoutView);
    });

    it('should render LayoutView', () => {
        expect(document.body).toContainElement(app.rootView.el);
    });
});
