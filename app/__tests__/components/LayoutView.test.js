import Stickyfill from 'stickyfilljs';
import { LayoutView } from 'components/LayoutView';
import { NavBarView } from 'components/NavBarView';
import { SearchView } from 'components/SearchView';
import { CurrenciesView } from 'components/CurrenciesView';

const SEARCH_STRING = 'search';
const TRIGGERED_EVENT_NAME = 'search:input:change';
const layoutView = new LayoutView();

beforeAll(() => {
    layoutView.render();
});

describe('LayoutView', () => {
    it('should populate navBarRegion with NavBarView', () => {
        const navBarView = layoutView.navBarRegion.currentView;

        expect(navBarView).toBeInstanceOf(NavBarView);
        expect(layoutView.el).toContainElement(navBarView.el);
    });

    it('should populate searchRegion with SearchView', () => {
        const searchView = layoutView.searchRegion.currentView;

        expect(searchView).toBeInstanceOf(SearchView);
        expect(layoutView.el).toContainElement(searchView.el);
    });

    it('should populate currenciesRegion with CurrenciesView', () => {
        const currenciesView = layoutView.currenciesRegion.currentView;

        expect(currenciesView).toBeInstanceOf(CurrenciesView);
        expect(layoutView.el).toContainElement(currenciesView.el);
    });

    it('should make SearchRegion sticky', () => {
        const searchRegion = layoutView.searchRegion;
        const stickyNodes = Stickyfill.stickies.map(({_node}) => _node);

        expect(stickyNodes).toContain(searchRegion.el);
    });

    it('should unstick SearchRegion before destroying it', () => {
        const tmpLayoutView = new LayoutView();
        tmpLayoutView.render();
        const searchRegion = tmpLayoutView.searchRegion;
        const stickyNodesBeforeDestroy = Stickyfill.stickies.map(({_node}) => _node);
        expect(stickyNodesBeforeDestroy).toContain(searchRegion.el);

        tmpLayoutView.destroy();

        const stickyNodesAfterDestroy = Stickyfill.stickies.map(({_node}) => _node);
        expect(stickyNodesAfterDestroy).not.toContain(searchRegion.el);
    });

    it(`should filter currencies region on ${TRIGGERED_EVENT_NAME} child event`, () => {
        const searchView = layoutView.searchRegion.currentView;
        const currenciesView = layoutView.currenciesRegion.currentView;
        const filterSpy = jest.spyOn(currenciesView, 'filterCurrencies');

        searchView.triggerMethod(TRIGGERED_EVENT_NAME, SEARCH_STRING);

        expect(filterSpy).toHaveBeenCalledWith(SEARCH_STRING);
    });
});
