const _ = require('lodash');

module.exports = {
    process(src) {
        return `module.exports = ${_.template(src, { variable: 'data' })}`;
    }
};
