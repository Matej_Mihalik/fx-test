# Welcome to the George Front-End Team test day!

Your task will be to create a small one page application that displays
all the currencies from the Bank and the related exchange rate.

## Specification
The page should have a header and a search bar.

When the user is scrolling the page, the header should scroll off the
page but the search bar should stick to the top and stay there.

Below the header and the search bar should be a list of the currencies.

A list item should contain
- Flag of the country
- Name of the country
- Currency of the country
- Exchange rates of the currency

The currencies should be searchable by name and by abbreviation.
The list should update according to the search results.

## Resources

API URL for currencies: https://localhost:8011/fat1/rest/info/fx

API documentation: presto.html

Wire-frame: wire-frame.jpg

Pictures to display the flags: flags/*.png

Country information: countries.json

## Local proxy for the API request

To run the local proxy you will need to install the required packages:

    npm install

After that to start the proxy use this command:

    node startProxy.js

After this you can reach the API at the address:
https://localhost:8011/fat1/rest/info/fx

According to your server settings you might need to adjust the settings
in the file **startProxy.js**.
(Don't forget to restart the proxy after changing the address!)

## Notes

You can use whatever tools and resources you want. The application can
be based on *BackboneJS*, *MarionetteJS*, *React* or any other library
or framework however the language should be **JavaScript**.

The application source code should be in production ready state.
Implement it the best way you can (clean modules, jsdoc, tests ...).

The application should look nice ;)

When you finished the application, we will ask you to present it,
including the source code.

If you have ANY questions, don't hesitate to ask! You can also ask for
help and clarification.

Good luck and have fun!