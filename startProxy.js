// FIX SSL
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

// Proxy Settings
var originUrl = "http://localhost:63342",
    proxyPort = 8011;

var fs = require('fs'),
    http = require('http'),
    https = require('https'),
    httpProxy = require('http-proxy');

function startServer () {

    var proxyOptions = {

        target: 'https://api.fat.sparkasse.at',

        agent: https.globalAgent,
        headers: {
            host: 'api.fat.sparkasse.at'
        }
    };

    var proxy = httpProxy.createProxyServer(proxyOptions).listen(proxyPort);

    proxy.on("start", function (req) {
        if (req.headers.cookie) {
            req.headers.cookie = req.headers.cookie.replace('PROXYSESSIONID', 'DONTSENDTHISSESSIONID');
        }
    });

    proxy.on('error', function (err, req, res) {
        console.error("Presto Proxy : ERROR : ", err);

        res.writeHead(500, {
            'Content-Type': 'text/plain'
        });

        res.end('Something went wrong. And we are reporting a custom error message.');
    });

    proxy.on("proxyRes", function (proxyRes, req, res) {
        proxyRes.headers["Access-Control-Allow-Origin"] = originUrl;

        proxyRes.headers["Access-Control-Allow-Methods"] = "GET, POST, PUT, DELETE, OPTIONS";
        proxyRes.headers["Access-Control-Allow-Headers"] = "X-Requested-With, X-REQUEST-ID, X-Prototype-Version,X-GEORGE-USER, Content-Type, Origin, Allow, cookieCheck, Authorization";
        proxyRes.headers["Access-Control-Allow-Credentials"] = "true";
        proxyRes.headers["Access-Control-Max-Age"] = 6000;

        if (proxyRes.headers && proxyRes.headers["set-cookie"] && proxyRes.headers["set-cookie"][0]) {
            proxyRes.headers['set-cookie'][0] = proxyRes.headers['set-cookie'][0].replace(/HttpOnly=/gi, "");
            proxyRes.headers['set-cookie'][0] = proxyRes.headers['set-cookie'][0].replace(/JSESSIONID=/gi, "JAPISESSIONID=");
        }
    });

    console.log("Proxy: Server started (Port 8011) against " + proxyOptions.target + "!");
}

startServer();
